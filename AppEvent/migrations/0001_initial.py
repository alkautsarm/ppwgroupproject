from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EventModels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('judul', models.CharField(default='', max_length=50)),
                ('deskripsi', models.CharField(default='', max_length=200)),
                ('tanggal', models.DateTimeField()),
                ('gambar', models.FileField(default='', upload_to='')),
            ],
        ),
    ]
