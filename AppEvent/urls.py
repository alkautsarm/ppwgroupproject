from django.urls import path

from . import views

urlpatterns = [
	path('event-page', views.eventPage, name='event_page'),
	path('event-page/list', views.eventList, name='event_page_list'),
	path('event-page/list/preview-1', views.eventPreview, name='event_page_preview1'),
	path('event-page/list/preview-2', views.eventPreview, name='event_page_preview2'),
	path('event-page/list/preview-3', views.eventPreview, name='event_page_preview3'),
	path('event-page/list/preview/registered-participants', views.registeredParticipants, name='event_page_preview'),
	path('api/event/data', views.registerUserToEvent, name='event_api'),
]