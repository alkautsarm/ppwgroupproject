from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from .models import EventModels
from AppAudEvent.models import Joined_Person

# Create your views here.
def eventPage(request) :
	return render(request, 'event_page.html')

def eventList(request) :
	return render(request, 'eventpage_list.html')

def eventPreview(request) :
	list_events = EventModels.objects.all().values()
	response = {}

	#Cek link yang sedang diakses sehingga akan menampilkan info-info tertentu
	if(request.path == '/event-page/list/preview-1') :
		response = {'listevent':list_events[:1], 
		'image':'/static/AppEvent/event-1.jpg'}
	elif(request.path == '/event-page/list/preview-2') :
		response = {'listevent':list_events[1:2],
		'image':'/static/AppEvent/event-2.jpg'}
	elif(request.path == '/event-page/list/preview-3') :
		response = {'listevent':list_events[2:],
		'image':'/static/AppEvent/event-3.jpg'}

	return render(request, 'eventpage_preview.html', response)

def registeredParticipants(request) :
	'''	##UNTUK YG KMRN REGISTEREDPARTICIPANTS
	response = {}
	if("preview-1" in request.META.get('HTTP_REFERER', '')) :
		response = {'listuser' : Joined_Person.objects.filter(event = 1), 
		'back' : '\\event-page\\list\\preview-1',
		'event' : EventModels.objects.all().values()[:1][0]['judul']}
	elif ("preview-2" in request.META.get('HTTP_REFERER', '')) :
		response = {'listuser' : Joined_Person.objects.filter(event = 2),
		'back' : '\\event-page\\list\\preview-2',
		'event' : EventModels.objects.all().values()[1:2][0]['judul']}
	elif("preview-3" in request.META.get('HTTP_REFERER', '')) :
		response = {'listuser' : Joined_Person.objects.filter(event = 3),
		'back' : '\\event-page\\list\\preview-3',
		'event' : EventModels.objects.all().values()[2:][0]['judul']}
	else :
		response = {'back' : '\\event-page\\list'}
	'''
	return render(request, 'eventpage_registeredparticipants.html')

def registerUserToEvent(request) :
	nameOfEvent = request.GET.get('eventName', None)
	eventNow = EventModels.objects.filter(judul=nameOfEvent)
	response = {'data': 'Success'}

	if(len(eventNow) > 0) :
		if(eventNow[0].event.all().filter(email=request.user.email).exists()):
			response['data'] = 'alreadyexist'
		else:
			eventNow[0].event.add(request.user)

	return JsonResponse(response)