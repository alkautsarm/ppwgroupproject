from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class EventModels(models.Model):
	judul = models.CharField(max_length = 50, default = '')
	deskripsi = models.CharField(max_length = 200, default = '')
	tanggal = models.DateTimeField()
	gambar = models.FileField(default = '')
	event = models.ManyToManyField(User, blank = True)
