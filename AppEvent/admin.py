from django.contrib import admin

from .models import EventModels

# Register your models here.
admin.site.register(EventModels)