from django.test import TestCase
from django.http import HttpRequest

from .views import eventPage, eventList, eventPreview, registeredParticipants, registerUserToEvent
from .models import EventModels

#Structure :
#1 TESTING FOR EVERY PAGE EXIST
#2 TESTING FOR EVERY PAGE USING TEMPLATES

class EventPagesTest(TestCase) :
	
	def test_event_page_exist(self) :
		response = self.client.get('/event-page')
		self.assertEqual(response.status_code, 200)

	def test_event_page_using_templates(self) :
		response = self.client.get('/event-page')
		self.assertTemplateUsed(response, 'event_page.html')

	# def test_button_redirects_to_list_page(self) :
	# 	request = HttpRequest()
	# 	response = eventPage(request)
	# 	html = response.content.decode('utf8')
	# 	self.assertIn('href="\\event-page\\list', html)

class EventListPage(TestCase) :

	def test_event_list_page_exist(self) :
		response = self.client.get('/event-page/list')
		self.assertEqual(response.status_code, 200)

	def test_event_list_page_using_templates(self) :
		response = self.client.get('/event-page/list')
		self.assertTemplateUsed(response, 'eventpage_list.html')

	def test_button_redirects_to_3_event_preview_pages(self) :
		request = HttpRequest()
		response = eventList(request)
		html = response.content.decode('utf8')
		self.assertIn('href="\\event-page\\list\\preview-1"', html)
		self.assertIn('href="\\event-page\\list\\preview-2"', html)
		self.assertIn('href="\\event-page\\list\\preview-3"', html)

class EventPreviewPage(TestCase) :

	def test_events_preview_page_exist(self) :
		response = self.client.get('/event-page/list/preview-1')
		self.assertEqual(response.status_code, 200)

		response = self.client.get('/event-page/list/preview-2')
		self.assertEqual(response.status_code, 200)

		response = self.client.get('/event-page/list/preview-3')
		self.assertEqual(response.status_code, 200)

	def test_event_preview_page_using_templates(self) :
		response = self.client.get('/event-page/list/preview-1')
		response2 = self.client.get('/event-page/list/preview-2')
		response3 = self.client.get('/event-page/list/preview-3')
		self.assertTemplateUsed(response, 'eventpage_preview.html')
		self.assertTemplateUsed(response2, 'eventpage_preview.html')
		self.assertTemplateUsed(response3, 'eventpage_preview.html')

	def test_event_preview_page_has_redirects_button_to_regispart_formbook_back(self) :
		request = HttpRequest()
		response = eventPreview(request)
		html = response.content.decode('utf8')
		self.assertIn('href="\\event-page\\list"', html)
		self.assertIn('href="\"', html)
		self.assertIn('href="\"', html)

class EventRegisteredParticipantsPage(TestCase) :

	def test_event_registered_participants_page_exist(self) :
		response = self.client.get('/event-page/list/preview/registered-participants')
		self.assertEqual(response.status_code, 200)

	def test_event_registered_participants_page_using_templates(self) :
		response = self.client.get('/event-page/list/preview/registered-participants')
		self.assertTemplateUsed(response, 'eventpage_registeredparticipants.html')

class ModelsTesting(TestCase) :

	def test_models(self) :
		new_events = EventModels.objects.create(judul = 'The A', 
			deskripsi = 'The A adalah A', tanggal = '2018-10-18 14:30',
			gambar = '')
		collect = EventModels.objects.all().count()
		self.assertEqual(collect, 1)

class DaftarEventHarusLogin(TestCase) :

	def test_page_api_events_for_user_exist(self):
		response = self.client.get('/api/event/data')
		self.assertEqual(response.status_code, 200)

	


