from django.db import models
from django.utils import timezone 
from datetime import datetime, date

# Create your models here.
class all_comments(models.Model):
    name = models.CharField(max_length=50)
    time = models.DateTimeField(default=timezone.now())
    comment = models.TextField(max_length = 350)
def __str__(self):
    return self.comment
