from django.apps import AppConfig


class AppaboutConfig(AppConfig):
    name = 'AppAbout'
