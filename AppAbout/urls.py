from django.urls import path
from . import views

urlpatterns = [
    path('comment/', views.comment, name='comment'),   
    path('comment/share-comment/', views.share, name='share'),

]