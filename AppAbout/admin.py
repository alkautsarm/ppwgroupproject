from django.contrib import admin

from .models import all_comments

# Register your models here.
admin.site.register(all_comments)