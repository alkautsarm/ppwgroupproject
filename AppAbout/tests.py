from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import comment
from .models import all_comments
from .forms import about
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
import time
import unittest


# Create your tests here.
class UnitTest_About(TestCase):

    def test_about_url_exist(self):
        response = Client().get('/about/comment/')
        self.assertEqual(response.status_code, 200)

    def test_greet_is_exist(self):
        request = HttpRequest()
        response = comment(request)
        html_response = response.content.decode('utf8')
        self.assertIn('About Skoole.', html_response)
    
    def test_about_using_form_func(self):
        found = resolve('/about/comment/')
        self.assertEqual(found.func, comment)

    def test_appAbout_template(self):
        response = Client().get('/about/comment/')
        self.assertTemplateUsed(response, 'aboutPage.html')

    def test_can_share_comment(self): 
        comments= all_comments.objects.all().create(comment = "web ini bagus")
        count_comments = all_comments.objects.all().count()
        self.assertEqual(count_comments, 1)
