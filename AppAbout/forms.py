from django import forms
class about(forms.Form):
    invalid_message={
       'required' : 'Please fill out this field',
       'invalid' : 'Please fill out with valid input'
    }
    message_attrs={
        'type' : 'text',
        'class' : 'form-control',
        'id' : 'share-comment',
     
        
    }

    comment = forms.CharField(label = 'Share it to the world', widget=forms.Textarea(attrs=message_attrs),
       required=True,max_length=400)
