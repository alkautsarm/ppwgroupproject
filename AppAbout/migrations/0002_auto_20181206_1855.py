# Generated by Django 2.1.2 on 2018-12-06 11:55

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('AppAbout', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='all_comments',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 6, 11, 55, 43, 527532, tzinfo=utc)),
        ),
    ]
