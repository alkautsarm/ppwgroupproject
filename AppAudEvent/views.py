from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

from .forms import Form_Event
from .models import Joined_Person
from AppEvent.models import EventModels
import json

response = {}


def formEvent(request):
    form = Form_Event(request.POST or None)
    #allField = Joined_Person.objects.all()
    #response = {'allPerson' : allPerson}
    response['form'] = form

    if(request.method == 'POST' and form.is_valid()):
        #username = request.POST.get('username')
        #password = request.POST.get('password')
        #email = request.POST.get('email')
        #Joined_Person.objects.create(username = username,
         #                            password = password,
          #                           email = email)
        #return redirect('success')

        response['username'] = request.POST['username']
        response['password'] = request.POST['password']
        response['email'] = request.POST['email']
        items = Joined_Person(username=response['username'],
                              password=response['password'],
                              email=response['email']
                              )
        items.save()
        items = Joined_Person.objects.all()
        response['items'] = items
        return redirect('success')
    else:
        return render(request, 'AppAudEvent/formEvent.html', response)


def success(request):
    return render(request, 'AppAudEvent/success.html', response)


def participated_event(request):

    if request.user.is_authenticated:
        data = {}
        count = 0
        totalevent = EventModels.objects.count()
        for i in range(0, totalevent):
            a = EventModels.objects.filter(id=i+1)
            if(a[0].event.all().filter(email=request.user.email).exists()):
                data[i]['judul'] = a[0]['judul']
                data[i]['waktu'] = a[0]['waktu']
                count += 1
            else:
                pass
        return render(request, 'AppAudEvent/participatedEvents.html', {'data': data, 'count':count})
    else:
        return render(request, 'AppAudEvent/participatedEvents.html', {'isLog': False})
