from django.urls import path

from . import views

urlpatterns = [
    path('', views.formEvent, name='formEvent'),
    path('success/', views.success, name='success'),
] 