# Generated by Django 2.1.1 on 2018-12-13 14:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('AppAudEvent', '0002_joined_person_event'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='joined_person',
            name='event',
        ),
    ]
