from django.apps import AppConfig


class AppaudeventConfig(AppConfig):
    name = 'AppAudEvent'
