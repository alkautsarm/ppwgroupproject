from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

from .views import formEvent
from .models import Joined_Person
from .forms import Form_Event


class StatusPageTest(TestCase):
    def test_form_event_url_exists(self):
        response = self.client.get('/register-event/')
        self.assertEqual(response.status_code, 200)

    def test_participated_event_url_exists(self):
        response = self.client.get('/user/participated-events/')
        self.assertEqual(response.status_code, 200)

    def test_form_event_page_returns_correct_html(self):
        response = self.client.get('/register-event/')
        self.assertTemplateUsed(response, 'AppAudEvent/formEvent.html')

    def test_participated_event_page_returns_correct_html(self):
        response = self.client.get('/user/participated-events/')
        self.assertTemplateUsed(
            response, 'AppAudEvent/participatedEvents.html')

    def test_can_save_a_POST_request_in_form_event(self):
        # Creating a new status
        new_status = Joined_Person.objects.create(username='test',
                                                  password='testtestq',
                                                  email='test@test.com')

        # Retrieving all available activity
        counting_all_available_person = Joined_Person.objects.all().count()
        self.assertEqual(counting_all_available_person, 1)

    def test_form_validation_for_blank_items_in_form_event(self):
        form = Form_Event(data={'username': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'],
            ["This field is required."]
        )

    def test_form_validation_for_password_less_than_8_in_form_event(self):
        form = Form_Event(data={'password': 'wqdq'})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password'],
            ["Password must at least 8 characters"]
        )

    def test_form_validation_for_password_not_alphanumeric_in_form_event(self):
        form = Form_Event(data={'password': '%&^&%&^&^'})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password'],
            ["Password must not contain any character except alphanumeric"]
        )

    def test_redirects_after_POST_in_form_event(self):
        response = self.client.post('/register-event/', data={'username': 'test',
                                                              'password': 'testtestq',
                                                              'email': 'test@test.com'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/register-event/success/')

    def test_only_saves_items_when_necessary_in_form_event(self):
        self.client.get('/register-event/')
        self.assertEqual(Joined_Person.objects.count(), 0)
