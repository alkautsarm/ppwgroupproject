from django import forms
from django.core.validators import validate_email
from .models import Joined_Person

from AppEvent.models import EventModels

class Form_Event(forms.ModelForm):
    invalid_message = {
        'required' : 'Please fill out this field',
        'invalid' : 'Please fill out with valid input',
    }
    username_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your username',
    }
    password_attrs={
        'type' : 'password',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your password',
    }
    email_attrs={
        'type' : 'email',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your valid e-mail',
    }
    
    '''##UNTUK YG KMRN REGISTEREDPARTICIPANTS
    eventModelsQuery = EventModels.objects.all().values()
    event_choices=(
        (1, (eventModelsQuery[:1][0]['judul'])),
        (2, (eventModelsQuery[1:2][0]['judul'])),
        (3, (eventModelsQuery[2:][0]['judul']))
    )
    '''

    username = forms.CharField(
        label='Username:', 
        max_length=70,
        required=True, 
        widget=forms.TextInput(attrs = username_attrs)
        )

    password = forms.CharField(
        label='Password:',
        max_length=70,
        required=True,
        widget=forms.PasswordInput(attrs = password_attrs)
        )

    email = forms.EmailField(
        label='E-Mail:',
        max_length=70,
        required=True,
        widget=forms.EmailInput(attrs = email_attrs)
        )
    ##event = forms.ChoiceField(choices = event_choices)

    class Meta():
        model = Joined_Person
        fields = ['username', 'password', 'email']

    def clean_username(self):
        user = self.cleaned_data['username']
        items = Joined_Person.objects.all()
        for name in items:
            if user == name.username:
                raise ValidationError('Username Already Exist')
        return user

    def clean_password(self):
        pas = self.cleaned_data['password']
        MIN_LENGTH = 8
        if pas:
            if len(pas) < MIN_LENGTH:
                raise forms.ValidationError('Password must at least 8 characters')
            for letter in pas:
                if not letter.isalnum():
                    raise forms.ValidationError('Password must not contain any character except alphanumeric')
            return pas

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            mt = validate_email(email)
        except:
            return forms.ValidationError('Email is incorret')
        return email