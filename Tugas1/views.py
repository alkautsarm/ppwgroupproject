from django.contrib.auth import logout
from django.http import HttpResponseRedirect


def logoutPage(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/')
