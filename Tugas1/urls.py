"""Tugas1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, re_path, include
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from AppNews.views import contacts as contacts
from AppNews.views import faq as faq
from .views import logoutPage
from AppAudEvent.views import participated_event

urlpatterns = [
    path('admin/', admin.site.urls),
    path('news/', include('AppNews.urls')),
    path('',include('AppMember.urls')),
    path('', include('AppEvent.urls')),
    path('register-event/', include('AppAudEvent.urls')),
    path('user/participated-events/',
         participated_event, name='participated-event'),
    path('about/',include('AppAbout.urls')),

    re_path('^contacts',contacts, name='contacts'),
    re_path('^faq',faq, name='faq'),


    # path('login/', views.LoginView.as_view(), name='login'),
    # path('logout/', views.logout, name='logout'),
    # url('auth/', include('social_django.urls', namespace='social')),  # <- Here
    re_path(r'^login', views.LoginView.as_view(), name="login"),
    re_path(r'^logout', logoutPage, name="logout"),
    re_path(r'^auth', include('social_django.urls', namespace='social')),

]
