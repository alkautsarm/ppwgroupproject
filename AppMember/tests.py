from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import member
from .views import landingpage
from .views import result
from .models import form_member
from .forms import member_form
import unittest

class UnitTest_TP1(TestCase):

    def test_landingpage_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_member_url_exist(self):
        response = Client().get('/member/')
        self.assertEqual(response.status_code, 200)

    def test_result_url_exist(self):
        response = Client().get('/result/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/member/')
        self.assertEqual(found.func, member)
        
    # def test_can_create_new_member(self):
    #     regist_post= Client().post('/member/',{'fullname' : 'ini budi','username' : 'budibudi','dob' : '2000-10-10','email':'budi@gmail.com','password':'inibudiini','address':'fasilkom' })
    #     self.assertEqual(regist_post.status_code,302)
    #     counting_all_regist = form_member.objects.all().count()
    #     self.assertEqual(counting_all_regist,1) 
 
    def test_TP_using_template(self):
        response =Client().get('/member/')
        self.assertTemplateUsed(response, 'registerMember.html')

    def test_registerMember_page_use_form(self):
        response= self.client.get('/member/')   
        self.assertIsInstance(response.context['registration'],member_form)  

    def test_form_validation_for_blank_items(self):
        form = member_form(data={'username' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'][0],
            'This field is required.'
        )
 
