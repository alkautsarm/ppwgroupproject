from django import forms
class member_form(forms.Form):
    invalid_message={
       'required' : 'Please fill out this field',
       'invalid' : 'Please fill out with valid input'
    }
    name_attrs={
       'type' : 'text',
       'class' : 'todo-form-input',
       'placeholder' : 'Please enter your fullname',

    }
    username_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please make a username',
    }
    dob_attrs={
        'type' : 'date',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your date of birth',
    }
    email_attrs={
        'type' : 'email',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your valid email',
    }
    password_attrs={
        'type' : 'password',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your password',
    }

    address_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your address',
    }



    name = forms.CharField(label='Full Name:',max_length=60, required=True,
        widget=forms.TextInput(attrs = name_attrs))
    username = forms.CharField(label = 'Username:',max_length=60,required=True,
        widget=forms.TextInput(attrs=username_attrs))
    dob = forms.DateField(label = 'Date of Birth:', required=True,
        widget=forms.DateInput(attrs=dob_attrs))
    email = forms.EmailField(label = 'Email:',max_length = 60, required=True, 
        widget=forms.EmailInput(attrs=email_attrs))
    password = forms.CharField(label = 'Password:',required=True,
        widget=forms.PasswordInput(attrs=password_attrs))
    address = forms.CharField(label = 'Address:',required=True,
        widget=forms.Textarea(attrs=address_attrs))