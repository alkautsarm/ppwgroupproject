from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import member_form
from .models import form_member


# Create your views here.
response = {}
def landingpage (request):
     return render(request, "landingpage.html", response)
def member(request):
    registration = member_form (request.POST or None)
    allMessage = form_member.objects.all()
    response = {
        "allMessage" : allMessage
    }
    response['registration'] = registration
    if(request.method == "POST" and registration.is_valid()):
        name = request.POST.get('name')
        username = request.POST.get('username')
        dob = request.POST.get('dob')
        email = request.POST.get('email')
        password = request.POST.get('password')
        address = request.POST.get('address')
        form_member.objects.create(name = name, username=username, dob=dob, email=email, password=password, address=address)
        return redirect('result')
    else:
        # response["registration"]=registration
        return render(request, "registerMember.html", response)
        
def result(request):
     return render(request, "registered.html", response)
