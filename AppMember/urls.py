from django.urls import path

from . import views

urlpatterns = [
    path('', views.landingpage, name='landingpage'),
    path('member/', views.member, name='member'),
    path('result/', views.result, name='result'),
] 