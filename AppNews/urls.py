from django.urls import path

from .views import frontnews, appnews, contacts, faq

urlpatterns = [
	path('', frontnews, name='frontnews'), #path(route, view, ...)
	path('details/', appnews, name='appnews'), #path(route, view, ...)
	path('', contacts, name='contacts'), #path(route, view, ...)
	path('', faq, name='faq'), #path(route, view, ...)
]