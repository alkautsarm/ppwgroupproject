from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from datetime import datetime, date

# Create your models here.

class NewsModelEntertainment(models.Model):
    title = models.CharField(max_length=300)
    date = models.CharField(max_length=30)
    description = models.TextField(max_length=200)
    photo = models.CharField(max_length = 500)
    seemore = models.CharField(max_length = 1000)

class NewsModelTravel(models.Model):
    title = models.CharField(max_length=300)
    date = models.CharField(max_length=30)
    description = models.TextField(max_length=200)
    photo = models.CharField(max_length = 500)
    seemore = models.CharField(max_length = 1000)

class NewsModelSport(models.Model):
    title = models.CharField(max_length=300)
    date = models.CharField(max_length=30)
    description = models.TextField(max_length=200)
    photo = models.CharField(max_length = 500)
    seemore = models.CharField(max_length = 1000)

class NewsModelFood(models.Model):
    title = models.CharField(max_length=300)
    date = models.CharField(max_length=30)
    description = models.TextField(max_length=200)
    photo = models.CharField(max_length = 500)
    seemore = models.CharField(max_length = 1000)