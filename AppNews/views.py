from django.shortcuts import render
from .models import NewsModelEntertainment, NewsModelTravel, NewsModelSport, NewsModelFood

# Create your views here.
response = {}

def frontnews(request):
    return render(request, 'frontnews.html', response)

def appnews(request):
    newsentertainment = NewsModelEntertainment.objects.all()
    response['newsentertainment'] = newsentertainment
    newstravel = NewsModelTravel.objects.all()
    response['newstravel'] = newstravel
    newssport = NewsModelSport.objects.all()
    response['newssport'] = newssport
    newsfood = NewsModelFood.objects.all()
    response['newsfood'] = newsfood
    return render(request, 'appnews.html', response)

def contacts(request):
    return render(request, 'contacts.html', response)

def faq(request):
    return render(request, 'faq.html', response)