from django.test import TestCase
from django.test import Client
from django.urls import resolve
from AppNews.models import NewsModelEntertainment, NewsModelTravel, NewsModelSport, NewsModelFood
from .views import frontnews, appnews


# Create your tests here.
class NewsTest(TestCase):
    def test_page_exist(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_news_page_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)
    
    def test_faq_page_exist(self):
        response = Client().get('/faq')
        self.assertEqual(response.status_code, 200)

    def test_contacts_page_exist(self):
        response = Client().get('/contacts')
        self.assertEqual(response.status_code, 200)

    def test_using_frontnews_method(self):
        found = resolve('/news/')
        self.assertEqual(found.func, frontnews)

    def test_using_appnews_method(self):
        found = resolve('/news/details/')
        self.assertEqual(found.func, appnews)

    def test_has_news_models_entertainment(self):
        NewsModelEntertainment.objects.create(title="test", date="tomorrow, 20xx", description="skippp", photo="https://i.imgur.com/k35oP9p.jpg")
        count_all_stats = NewsModelEntertainment.objects.all().count()
        self.assertEqual(count_all_stats, 1)
    
    def test_has_news_models_travel(self):
        NewsModelTravel.objects.create(title="test", date="tomorrow, 20xx", description="skippp", photo="https://i.imgur.com/k35oP9p.jpg")
        count_all_stats = NewsModelTravel.objects.all().count()
        self.assertEqual(count_all_stats, 1)
    
    def test_has_news_models_sport(self):
        NewsModelSport.objects.create(title="test", date="tomorrow, 20xx", description="skippp", photo="https://i.imgur.com/k35oP9p.jpg")
        count_all_stats = NewsModelSport.objects.all().count()
        self.assertEqual(count_all_stats, 1)
    
    def test_has_news_models_food(self):
        NewsModelFood.objects.create(title="test", date="tomorrow, 20xx", description="skippp", photo="https://i.imgur.com/k35oP9p.jpg")
        count_all_stats = NewsModelFood.objects.all().count()
        self.assertEqual(count_all_stats, 1)

    #def test_landingpage_show_news(self):
    #   News.objects.create(judul="test", penulis="abscdlre", gambar="https://akcdn.detik.net.id/community/media/visual/2018/10/17/d464f46c-4711-4a1f-9a0b-14035b77d51d_169.jpeg?w=780&q=90", readmore="https://news.detik.com/berita/d-4260390/menkum-sebagian-besar-napi-tahanan-di-palu-sudah-kembali?_ga=2.53561896.1645967930.1539762383-1677352268.1526769961")
    #    response = Client().get('/')
    #    news_content = "Berita"
    #    html_response = response.content.decode('utf8')
    #    self.assertIn(news_content,html_response)
 
