from django.contrib import admin
from .models import NewsModelEntertainment, NewsModelTravel, NewsModelSport, NewsModelFood

# Register your models here.
admin.site.register(NewsModelEntertainment)
admin.site.register(NewsModelTravel)
admin.site.register(NewsModelSport)
admin.site.register(NewsModelFood)