# PPWGroupProject

PPW Group Project

Nama Anggota :

	Muhammad Al-Kautsar Maktub (1706043885)
	Alya Isti Safira (1706984511)
	Nadhira Shafa Thalia (1706043544)
	Firdhan Hilmy Purnomo (1706026992)

Link Herokuapp :
https://ppwgroupproject.herokuapp.com

Status Pipelines: 
[![pipeline status](https://gitlab.com/alkautsarm/ppwgroupproject/badges/master/pipeline.svg)](https://gitlab.com/alkautsarm/ppwgroupproject/commits/master)

Status Code Coverage:
[![coverage report](https://gitlab.com/alkautsarm/ppwgroupproject/badges/master/coverage.svg)](https://gitlab.com/alkautsarm/ppwgroupproject/commits/master)
